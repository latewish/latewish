using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float time;
    [SerializeField] float offset;
    private float startTime;
    private float journeyLength;
    [SerializeField] Vector3 StartPosition;
    [SerializeField] Vector3 EndPosition;
    [SerializeField] SpriteRenderer spriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
        journeyLength = Vector3.Distance(StartPosition, EndPosition);
        spriteRenderer = GetComponent<SpriteRenderer>();
        StartPosition = transform.position;
        EndPosition = new Vector3(transform.position.x, (transform.position.y + spriteRenderer.bounds.size.y) - offset, transform.position.z);
        
    }

    // Update is called once per frame
    void Update()
    {
        if(speed < 1) { 
        speed += Time.deltaTime / time;
        transform.position = Vector3.Lerp(StartPosition, EndPosition, speed);
        }

    }

}
