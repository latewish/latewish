using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/BurbujasSprites", order = 1)]
public class BurbujasSprites : ScriptableObject
{
    public Sprite BurbujaPequena;
    public Sprite BurbujaMediana;
    public Sprite BurbujaGrande;
    public Sprite BurbujaMala;
    public Sprite BurbujaMalaGrande;
    public Sprite[] RecuerdosBuenos;
    public Sprite[] RecuerdosMalos;
}
