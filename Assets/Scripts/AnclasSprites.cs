using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/AnclasSprites", order = 1)]
public class AnclasSprites : ScriptableObject
{
    public Sprite[] Anclas;
}
