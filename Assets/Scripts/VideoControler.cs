using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
public class VideoControler : MonoBehaviour
{
    [SerializeField] VideoClip[] videoClips;
    VideoPlayer video;
    // Start is called before the first frame update
    void Start()
    {
        video = GetComponent<VideoPlayer>();
        PlayAClip(0);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxisRaw("Vertical") < 0) PlayAClip(1);
        if (Input.GetAxisRaw("Vertical") > 0) PlayAClip(0);
        if (Input.GetAxisRaw("Horizontal") < 0) PlayAClip(2);
        if (Input.GetAxisRaw("Horizontal") > 0) PlayAClip(3);
    }

    void PlayAClip(int a)
    {
        video.clip = videoClips[a];
        
        if (video.isPrepared) {

            video.Play();

        }
    }
}
