using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public enum MyHeigh
{
    Small,
    Medium,
    Big
}

public class DragAndDrop : MonoBehaviour
{
    public static Action<GameObject, GameObject, bool,MyHeigh, MyHeigh> EspecificacionesDeTransformación;
    Camera Maincamera;
    Vector3 PointOfMouseOffset;
   
    Rigidbody2D rb;
    [SerializeField] MovementBurbuja movement;
    [SerializeField] float t = 1;
    [SerializeField] float force = 20f;
    [SerializeField] MyHeigh heigh = MyHeigh.Small;
    [SerializeField] bool IsBad = false;
    [SerializeField] bool IsDrag = false;
    [SerializeField] bool IsMerged = false;
    [SerializeField] bool IsSendingSignal = false;
    
    private void Awake()
    {
        Maincamera = Camera.main;
        rb = GetComponent<Rigidbody2D>();
        movement = GetComponent<MovementBurbuja>();
    }

    private Vector3 GetMousePosition()
    {
        return  Maincamera.ScreenToWorldPoint(Input.mousePosition);
    }
    private void OnMouseDown()
    {
        PointOfMouseOffset = gameObject.transform.position - GetMousePosition();
        IsDrag = true;
        movement.enabled = false;
        rb.velocity = Vector2.zero;
    }

    private void OnMouseDrag()
    {
        transform.position = Vector3.Lerp(transform.position, GetMousePosition() + PointOfMouseOffset, t * Time.deltaTime);
      
    }

    private void OnMouseUp()
    {
        rb.AddRelativeForce(Vector2.up*force, ForceMode2D.Force);
        IsDrag = false;
        movement.enabled = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {if (IsMerged) { 
        if (heigh == collision.gameObject.GetComponent<DragAndDrop>().heigh && !IsBad) 
        {  
            if (IsDrag)
             {
                     if (!collision.gameObject.GetComponent<DragAndDrop>().IsBad)
                     {
                          
                         EspecificacionesDeTransformación?.Invoke(gameObject, collision.gameObject, IsBad,heigh ,heigh);
                     }
             }
        }
        else if (IsBad && !IsDrag)
        {
            if (!IsSendingSignal && !collision.gameObject.GetComponent<DragAndDrop>().IsSendingSignal) {
                IsSendingSignal = true;
                collision.gameObject.GetComponent<DragAndDrop>().IsSendingSignal = false;
           if(IsSendingSignal) EspecificacionesDeTransformación?.Invoke(gameObject, collision.gameObject, IsBad,heigh ,collision.gameObject.GetComponent<DragAndDrop>().heigh);
            }
        }
        }
    }

   public void BeBad(bool a)
    {
        IsBad = a;
    }
    public void Crece(MyHeigh a)
    {
        heigh = a;
    }
    public void CanBeMerged(bool a)
    {
        IsMerged = a;
    }
}
