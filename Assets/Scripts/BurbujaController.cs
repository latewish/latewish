using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class BurbujaController : MonoBehaviour
{
    public static event Action<bool> GoodorBad;
    [SerializeField] float t= 0.5f;
    [SerializeField] AudioClip[] audios;
    [SerializeField] GameObject burbuja;
    [SerializeField] BurbujasSprites bu;
    [SerializeField] GameObject particles;
    private void OnEnable()
    {
        DragAndDrop.EspecificacionesDeTransformación += TransformacionBurbuja;
    }
    private void OnDisable()
    {
        DragAndDrop.EspecificacionesDeTransformación -= TransformacionBurbuja;
       
    }

    void TransformacionBurbuja(GameObject a, GameObject b, bool c, MyHeigh d, MyHeigh e)
    {
        GameObject NuevaBurbuja;
        GameObject BurbujasParticles;
        Vector3 PosicionNuevaBurbuja;
        DragAndDrop CambiosDeLaBurbuja;
            switch (d)
            {
                case MyHeigh.Small:
                    PosicionNuevaBurbuja = Vector3.Lerp(a.transform.position, b.transform.position, t);
                    BurbujasParticles = Instantiate(particles, PosicionNuevaBurbuja, a.transform.rotation);
                    BurbujasParticles.transform.localScale = new Vector3(BurbujasParticles.transform.localScale.x + 0.15f, BurbujasParticles.transform.localScale.y + 0.15f, BurbujasParticles.transform.localScale.z);
                    Destroy(BurbujasParticles, 3);
                    NuevaBurbuja = Instantiate(burbuja, PosicionNuevaBurbuja, a.transform.rotation);
                    NuevaBurbuja.transform.localScale = new Vector3(NuevaBurbuja.transform.localScale.x + 0.15f, NuevaBurbuja.transform.localScale.y + 0.15f, NuevaBurbuja.transform.localScale.z);
                    CambiosDeLaBurbuja =  NuevaBurbuja.GetComponent<DragAndDrop>();
                    CambiosDeLaBurbuja.Crece(MyHeigh.Medium);
                    
                    if (c)
                    {
                        CambiosDeLaBurbuja.BeBad(c);
                        NuevaBurbuja.GetComponent<SpriteRenderer>().sprite = bu.BurbujaMala;
                    AudioManager.Instance.Playclip(audios[1], true);
                }
                    else
                    {
                        NuevaBurbuja.GetComponent<SpriteRenderer>().sprite = bu.BurbujaMediana;
                    AudioManager.Instance.Playclip(audios[0], true);
                }
                   
                    Destroy(a);
                    Destroy(b);
                    break;
                case MyHeigh.Medium:
                    PosicionNuevaBurbuja = Vector3.Lerp(a.transform.position, b.transform.position, t);
                    BurbujasParticles = Instantiate(particles, PosicionNuevaBurbuja, a.transform.rotation);
                    BurbujasParticles.transform.localScale = new Vector3(BurbujasParticles.transform.localScale.x + 0.15f, BurbujasParticles.transform.localScale.y + 0.15f, BurbujasParticles.transform.localScale.z);
                    Destroy(BurbujasParticles, 3);
                    NuevaBurbuja = Instantiate(burbuja, PosicionNuevaBurbuja, a.transform.rotation);
                    NuevaBurbuja.transform.localScale = new Vector3(NuevaBurbuja.transform.localScale.x + 0.15f, NuevaBurbuja.transform.localScale.y + 0.15f, NuevaBurbuja.transform.localScale.z);
                    CambiosDeLaBurbuja = NuevaBurbuja.GetComponent<DragAndDrop>();
                    
                if (c)
                    {
                        CambiosDeLaBurbuja.BeBad(c);
                        NuevaBurbuja.GetComponent<SpriteRenderer>().sprite = bu.BurbujaMala;
                      AudioManager.Instance.Playclip(audios[1],true);
                    if (e == MyHeigh.Medium ) {
                        CambiosDeLaBurbuja.Crece(MyHeigh.Big);
                        NuevaBurbuja.GetComponent<SpriteRenderer>().sprite = bu.BurbujaMalaGrande;
                        GoodorBad?.Invoke(false);
                    }
                    else if( e == MyHeigh.Small)
                    {
                        CambiosDeLaBurbuja.Crece(MyHeigh.Medium);
                    }
                    
                }
                    else
                    {
                        CambiosDeLaBurbuja.Crece(MyHeigh.Big);
                        NuevaBurbuja.GetComponent<SpriteRenderer>().sprite = bu.BurbujaGrande;
                    AudioManager.Instance.Playclip(audios[0], true);
                    GoodorBad?.Invoke(true);
                    }
                    Destroy(a);
                    Destroy(b);
                    break;
               
            }
       
        }
    }

