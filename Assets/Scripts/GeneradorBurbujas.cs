using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorBurbujas : MonoBehaviour
{
    [SerializeField] GameObject Burbuja;
    [SerializeField] BurbujasSprites bu;
    [SerializeField] Vector3 limites;
    [SerializeField] float mintime, maxtime;
    [SerializeField] float minvueltas, maxvueltas;
  
    List<GameObject> Burbujas = new List<GameObject>();
    [SerializeField] float time = 10;
    [SerializeField] int vueltas = 6;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (time > 0)
        {

            time -= Time.deltaTime;

        }
        else
        {
            
            for (int i = 0; i < vueltas; i++ )
            {
                Vector3 positionBurbuja = new Vector3(transform.position.x + Random.Range(-limites.x, limites.x), transform.position.y + Random.Range(-limites.y, limites.y), transform.position.z);
                GameObject burbuja = Instantiate(Burbuja, positionBurbuja, transform.rotation);
                if (Random.Range(0,10)>=6)
                {
                    burbuja.GetComponent<DragAndDrop>().BeBad(true);
                    burbuja.GetComponent<SpriteRenderer>().sprite = bu.BurbujaMala;
                }
               
            }
            vueltas = (int)Mathf.Round(Random.Range(minvueltas, maxvueltas));
            time = Random.Range(mintime, maxtime);
        }
    }
}
