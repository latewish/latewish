using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementBurbuja : MonoBehaviour
{
    [SerializeField] Vector3 Limite;
    [SerializeField] Rigidbody2D rb;
    [SerializeField] float time = 0.25f;
    [SerializeField] float speed = 10;
    [SerializeField] float movdirectionx = 0.25f;
    [SerializeField] float directiony = 1;
    [SerializeField] float maxtime, mintime;
    float directionx = 0.25f;
    DragAndDrop drop;
    float mindirectionx = 0f;
    
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
       
       drop = GetComponent<DragAndDrop>();
       
    }

    private void OnMouseUp()
    {
        directiony = 2.5f;
        time = 0.5f;
    }
    // Update is called once per frame
    void Update()
    {
        if (time > 0) {
           
            time -= Time.deltaTime;

        }
        else
        {
            
            mindirectionx = directionx;
            
            if (Random.Range(-0.5f, 0.5f) >=0)
            {
                directionx = movdirectionx;
            }
            else
            {
                directionx = -movdirectionx;
            }

            if(directiony != 1)
            {
                directiony = 1;
            }
            time = Random.Range(mintime, maxtime);
        }

        if(transform.position.y >= Limite.y)
        {
            Destroy(gameObject);
        }
        
        if(transform.position.y >= (-Limite.y)+1)
        {
            drop.CanBeMerged(true);
        }

        if (transform.position.x >= Limite.x) {
            directionx = -movdirectionx;
            time = 0.5f;
        }
        else if(transform.position.x <= -Limite.x)
        {
            directionx = movdirectionx;
            time = 0.5f;
        }
        
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(Mathf.Lerp(mindirectionx, directionx, speed), directiony);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        directionx = -directionx;
    }
}
