using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Recuerdos", order = 1)]
public class Recuerdos : ScriptableObject

{ 
    public Sprite[] Burbujas;
    public Sprite[] RecuerdosBuenos;
    public Sprite[] RecuerdosMalos;
    public string[] FrasesBuenas;
    public string[] FrasesMalas;
}

