using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance = null;
    public AudioSource source;
    [SerializeField] float minPitch, maxPitch;
    [SerializeField] float minVolume, maxVolume;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        } else if(Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this);
    }

    public void Playclip(AudioClip a, bool b)
    {
        if (b)
        {
            RandomMizedSound();
        }
        source.PlayOneShot(a);
       
    }
    void RandomMizedSound()
    {
        source.pitch = Random.Range(minPitch,maxPitch);
        source.volume = Random.Range(minVolume, maxVolume);
    }
}
