using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlDeAncla : MonoBehaviour
{

    [SerializeField] AnclasSprites anclas;
    [SerializeField] Image image;
    private void OnEnable()
    {
        WhatRecuerdo.Contador += ContarPuntos;
    }
    private void OnDisable()
    {
        WhatRecuerdo.Contador -= ContarPuntos;
    }
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        WhatAnclaIam();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void ContarPuntos(int a)
    {
        if (StaticVariables.puntos + a >=0) { 
        StaticVariables.puntos += a;
        WhatAnclaIam();
        }
    }
    void WhatAnclaIam()
    {
        image.sprite = anclas.Anclas[StaticVariables.puntos];
    }
}
