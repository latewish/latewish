using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WhatRecuerdo : MonoBehaviour
{
    public static Action<int> Contador;
    [SerializeField] AudioClip[] audio;
    [SerializeField] SpriteRenderer Burbuja, Recuerdo;
    [SerializeField] Recuerdos recuerdos;
    [SerializeField] Animator anim;
    int puntos;
    private void OnEnable()
    {
        BurbujaController.GoodorBad += QueRecuerdoSoy;
    }
    private void OnDisable()
    {
        BurbujaController.GoodorBad -= QueRecuerdoSoy;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void QueRecuerdoSoy(bool a)
    {
       
        if (a)
        {
            puntos = 1;
            Burbuja.sprite = recuerdos.Burbujas[0];
            Recuerdo.sprite = recuerdos.RecuerdosBuenos[UnityEngine.Random.Range(0, recuerdos.RecuerdosBuenos.Length)];
            AudioManager.Instance.Playclip(audio[0], false);
        }
        else
        {
            puntos = -1;
            Burbuja.sprite = recuerdos.Burbujas[1];
            Recuerdo.sprite = recuerdos.RecuerdosMalos[UnityEngine.Random.Range(0, recuerdos.RecuerdosMalos.Length)];
            AudioManager.Instance.Playclip(audio[1], false);
        }
        anim.SetTrigger("Recuerda");
    }
    public void PuntosQueDar()
    {
        Contador?.Invoke(puntos);
    }
}
